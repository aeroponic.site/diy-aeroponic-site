uint8_t volatile ee[4]={255,117,255,118}; // 1,2,3,4 - max values and checksum
uint8_t volatile cc[4];//={0,0,0,0}; // current values
uint8_t volatile CS; // seconds
/*
//stop the pcints completely
ISR(PCINT0_vect,ISR_NAKED) { // registers clobbered: r2,r11,r16,r17,r18,r26,r27 accessed: r25:r24 
asm volatile(
//"push r1\n"
"in r2,0x3f\n"//save SREG

// false triggering?
// wait 1s to react
// must confirm high pin signal through all 255 interrupts 25.5ms
"in r16,0x16\n"
"andi r16,0b00011110\n"
"brne 1f\n"
"ldi r24,0\n"
"rjmp 2f\n"

"1:\n" // ok got something - dec counter

// check r25:r24 - if r25:r24 is not null just dec the waiting counter and exit
"cpi r24,0\n"
"brne 1f\n"
//"cpi r25,0\n"
//"brne 1f\n"

"ldi r16,0b00000010\n" // init mask
"ldi r18,1\n" // addr: pins to check 1,2,3,4
"ldi r26,lo8(ee+1)\n" // ptr to max
"ldi r27,hi8(ee+1)\n"
"np:\n"
// check current pin
"in r11,0x16\n" // load PINB
"and r11,r16\n"
"breq 3f\n" // skip if pin is LOW
// pin is HIGH
"out 0x15,r1\n" // disable pin change interrupts on all pins
// load max
"ld r17,X\n"
//"dec r17\n"
//"subi r17,10\n"
"subi r17,2\n"
// check limits
"cpi r17,30\n"
"brsh 11f\n"
"ldi r17,60\n"
"11:\n"
"st X,r17\n"
//"ldi r21,0xff\n" //set flag to update EEPROM
// write it here!
//"out 0x15,r1\n" // clear all pcmsk - no interrupts here
"out 0x18,r1\n" // stop all pins
"out 0x17,r1\n" // all pins to input
"sbi 0x17,0\n" // pin 0 to output
    "w:\n"
    "sbic 0x1c,1\n" //wait that EEPE bit is not set
    "rjmp w\n"
    "out 0x1c,r1\n" // atomic ERASE and write
    "out 0x1e,r18\n" //EEARL=address
    "out 0x1d,r17\n" // EEDR
    "sbi 0x1c,2\n" // set bit 2 EEMPE
    "sbi 0x1c,1\n" // set bit 1 EEPE to start atomic erase&write to EEPROM    
"ldi r24,0xff\n"
"rjmp 2f\n"
"3:\n"
"lsl r16\n" // next pin's mask
"adiw r26,1\n" //next max ptr
"inc r18\n"
"cpi r18,5\n"
"brne np\n"
"rjmp 2f\n"

"1:\n"
"dec r24\n"
"2:\n"

// exit
"out 0x3f,r2\n"//restore SREG 
//"pop r1\n"
"reti\n"
:::"r16","r17","r18","r19","r30","r31");
}*/


ISR(TIM0_COMPA_vect,ISR_NAKED) { //registers clobbered: r2,r11,r12,r16,r17,r18,r19,r20,r26,r27,r28,r29 accessed: r25:r24 
asm volatile(
// every 0.05ms

  //"sbi 0x18,0\n" //ON
"out 0x32,r1\n" // tcnt0=0
"in r2,0x3f\n"//save SREG

"sbiw r24,1\n"
"brne nxt\n"
// 1second
"lds r16,CS\n"
"inc r16\n"
"sts CS,r16\n"
    "ldi r25,hi8(20000)\n" // r25:r24 counter to maintain seconds
    "ldi r24,lo8(20000)\n"
"nxt:\n"

//"in r17,0x16\n" // PINB

/* //nice 1s blinks
"lds r17,CS\n"
"ldi r18,0b00011111\n"
"out 0x17,r18\n"
"andi r17,0b00000001\n"
"brne 12f\n"
"out 0x17,r1\n"
"12:\n"*/

"ldi r16,0b00000010\n" // initial mask
"ldi r18,1\n" //4 pins 1,2,3,4
"ldi r26,lo8(ee)\n" // ptr to max
"ldi r27,hi8(ee)\n"
"ldi r28,lo8(cc)\n" // ptr to current counters
"ldi r29,hi8(cc)\n"

"NextPin:\n"
  "sbi 0x18,0\n" //ON
"ld r19,X+\n" // load max
"ld r20,Y\n" // load current counter
"mov r17,r16\n"
"com r17\n" // invert all bits
  "cbi 0x18,0\n" //OFF
"cpi r20,7\n" // 0.35ms ON
"brlo inc\n"
//"brne 1f\n"
// 7 and higher
// DARK

"in r11,0x18\n"
"mov r12,r11\n"
"or r11,r16\n" 
"out 0x18,r11\n"//ON short burst ~200ns
"and r12,r17\n"
"out 0x18,r12\n"//OFF

//"in r11,0x18\n" //OFF: 5 and higher
//"and r11,r17\n" //OFF: 5 and higher
//"out 0x18,r11\n"//OFF: 5 and higher 

//"in r11,0x17\n" //pin to input 
//"and r11,r17\n" //pin to input 
//"out 0x17,r11\n"//pin to input 

//"in r11,0x15\n" //pin change interrupt enable 
//"or r11,r16\n"  //pin change interrupt enable 
//"out 0x15,r11\n"//pin change interrupt enable 

"inc:\n"
"inc r20\n" // inc current counter
"cp r20,r19\n" // check with maximum
"brne upd\n"
"ldi r20,0\n" // clear current if maximum exceeded
// start LIGHT from 0 to 7
//"in r11,0x15\n" //pin change interrupt disable 
//"and r11,r17\n" //pin change interrupt disable 
//"out 0x15,r11\n"//pin change interrupt disable 

/*
// check current pin for HIGH signal right before setting it HIGH. if limit is 100x0.05ms then it is 200 times per second here
"in r11,0x16\n" //PINB
"and r11,r16\n"
"brne low\n"
// current pin is HIGH start or continue counting r15 to 0
"dec r15\n" //0xff to go
"brne on\n"
// finally 256 times of HIGH (1.28s if 200Hz) - decrease and update the limit
"ld r21,-X\n" //load limit
"subi r21,2\n"
"cpi r21,60\n"
"brsh store\n"
"ldi r21,110\n"
"store:\n"
"st X+,r21\n" // update maximum. current value is 0
// write to EEPROM
    "out 0x18,r1\n" // stop all pins
    "w:\n"
    "sbic 0x1c,1\n" //wait that EEPE bit is not set
    "rjmp w\n"
    "out 0x1c,r1\n" // atomic ERASE and write
    "out 0x1e,r18\n" //EEARL=address
    "out 0x1d,r21\n" // EEDR
    "sbi 0x1c,2\n" // set bit 2 EEMPE
    "sbi 0x1c,1\n" // set bit 1 EEPE to start atomic erase&write to EEPROM
//"rjmp on\n"
"low:\n"
"mov r15,r1\n"// reset the high counter if pin is low
"on:\n"

*/

"in r11,0x18\n" //ON: 0,1,2,3,4 //pin to output HIGH
"or r11,r16\n"  //ON: 0,1,2,3,4 //pin to output HIGH
"out 0x18,r11\n"//ON: 0,1,2,3,4 //pin to output HIGH

//"nop\n""nop\n""nop\n"
//"in r11,0x17\n" //pin to output HIGH
//"or r11,r16\n"  //pin to output HIGH
//"out 0x17,r11\n"//pin to output HIGH

"upd:\n"

"st Y+,r20\n" // update current counter
"lsl r16\n"
"inc r18\n"
"cpi r18,5\n"
"brne NextPin\n" //2,3,4 pins to go

"out 0x3f,r2\n"//restore SREG 
"reti\n"
:::"r16","r17","r18","r19","r30","r31");
}

// the setup function runs once when you press reset or power the board
void setup() {
  ADCSRA &= ~(1<<ADEN); //Disable ADC
  ACSR = (1<<ACD); //Disable the analog comparator
  DIDR0 = 0x3F; //Disable digital input buffers on all pins.
//  TCCR0A=0;
//  TCCR0B=0;
  // setup watchdog for system reset if wdr is not called within 125ms;
 // WDTCR=(1 << WDE)|(1 << WDP1)|(1 << WDP0); //125ms
 WDTCR=(1 << WDE); //16ms between watchdog reset command 
 //WDTCR=(1 << WDE)|(1 << WDP0); //32ms between watchdog reset command 
 //WDTCR=(1 << WDE)|(1 << WDP1); //64ms between watchdog reset command 

 //TCCR0A = (1 << WGM01);// CTC mode 
 TCCR0A = 0b00000010;// CTC mode 
 
  //TCCR2B = (1 << CS21); // prescaler 8
  //TCCR0B = (1<<CS01)|(1<<CS00); // prescaler 64
  TCCR0B = 2;//8
  //TCCR0B = 3;//64
  //TCCR0B = 4;//256
  //TCCR0B = 5;//1024 9600000/1024=9375=125*75 75 times per second
  TCNT0  = 0;//initialize counter value to 0
  // OCR0A = 99;// = (16*10^6) / (1*8) - 1 (must be <65536)
  //OCR0A = 59; // 2500 times per second
  //OCR0A = 14; // 10000 times per second
  //OCR0A = 149; // 9600000/256=37500 / 150 = 250 times per second (4ms)
  //OCR0A = 37; // 9600000/256=37500 / 38 = 986 times per second (~1ms)
  //OCR0A = 14; // 9600000/64=150000 / 15 = 10000 times per second (~0.1ms)
  //OCR0A = 8; // 9600000/1024=9375 / 9 = 1041 times per second (~1ms)
  //OCR0A = 5; // 9600000/8=1200000 / 6 = 200000 times per second (~5us)
  //OCR0A = 4; // 9600000/8=1200000 / 5 = 240000 times per second (~4.17us)
  //OCR0A = 4; // 9600000/64=150000 / 5 = 30000 times per second (~33.3us)
  //OCR0A = 2; // 9600000/64=150000 / 3 = 50000 times per second (~20us)
  //OCR0A = 29; // 9600000/64=150000 / 30 = 5000 times per second (~200us)
  //OCR0A = 119; // 9600000/8=1200000 / 120 = 10000 times per second (~0.1ms)
  //OCR0A = 119; // 9600000/8=1200000 / 120 = 10000 times per second (~0.1ms)
  OCR0A = 59; // 9600000/8=1200000 / 60 = 20000 times per second (~0.05ms)
  
MCUCR=0b00100000; // SleepEnable + sleep mode IDLE
cli();
  TIMSK0 = (1 << OCIE0A);
  GIMSK=0b00000000; // INT0 disabled | Pin change INT disbled
 //GIMSK=0b00100000; // INT0 disabled | Pin change INT enabled
 
asm volatile ( // used r9,r10,r21,r22,r23,r30,r31
//    "cli\n" // no interrupts from now on
//    "ldi r16,85\n"
    //"clr r25\n"
//    "in r16,0x31\n"
//    "subi r16,-10\n"
//    "out 0x31,r16\n" //set OSCCAL
//    "ldi r16,127\n"
//    "out 0x31,r16\n" //set OSCCAL to maximum

    "ldi r16,0b00011111\n\t" // pins 0,1,2,3,4 for OUTPUT
    "out 0x17,r16\n\t"
    "ldi r25,hi8(20000)\n" // r25:r24 counter to maintain seconds: 20000 timer interrupts per second
    "ldi r24,lo8(20000)\n"
//    "mov r15,r1\n"
        
  //  "in r21,0x16\n" //PINB - if any pin 1-4 is HIGH on startup - reset to defaults
  //  "andi r21,0b00011110\n"
  //  "brne WriteDefaults\n"

/*
//test ee
    "1:\n"
    "sbic 0x1c,1\n" //wait that EEPE bit is not set
    "rjmp 1b\n"
    "out 0x1c,r1\n" // atomic ERASE and write
    "ldi r21,0\n"
    "out 0x1e,r21\n" //EEARL=address    
    "ldi r21,10\n"
    "out 0x1d,r21\n" // EEDR
    //"in r0,0x3f\n"
    //"cli\n"
    "sbi 0x1c,2\n" // set bit 2 EEMPE
    "sbi 0x1c,1\n" // set bit 1 EEPE to start atomic erase&write to EEPROM    
    //"out 0x3f,r0\n"

  
    "1:\n"
    "sbic 0x1c,1\n"
    "rjmp 1b\n"
    "ldi r21,0\n"
    "out 0x1e,r21\n" //EEARL=address
    "sbi 0x1c,0\n" // set bit 0 EERE READ EEPROM at EEARL
    "in r22,0x1d\n"// EEDR
    "cpi r22,10\n"
    "brne 1f\n"
    "rjmp WriteDefaults\n" //if ok then long pause .ok but wierd
    
    //"breq WriteDefaults\n"
    //"brne WriteDefaults2\n"
"1:\n"
*/
    
/*
    // load ee
    "ldi r30,lo8(ee+1)\n"
    "ldi r31,hi8(ee+1)\n"
    "ldi r21,1\n" // counter/addr
    "wt:\n"
    "sbic 0x1c,1\n"
    "rjmp wt\n"
    "out 0x1e,r21\n" //EEARL=address
    "sbi 0x1c,0\n" // set bit 0 EERE READ EEPROM at EEARL
    "in r22,0x1d\n"// read data byte from EEPROM at address EEARL into EEDR
    //sanity check
    "cpi r22,60\n" // 3ms
    "brlo WriteDefaults\n"
    "cpi r22,111\n"// 5.5ms
    "brsh WriteDefaults\n"
    
    "st Z+,r22\n"    
    "inc r21\n"
    "cpi r21,5\n"
    "brne wt\n" //2,3,4    
    */
    "Next:\n\t" 
    "sei\n"    
    "wdr\n"
    "sleep\n" // sleep until next timer (~0.005ms) interrupt came 
    "rjmp Next\n"
    /*
    //"cpi r21,0\n\t"
    //"breq Next\n\t"
    // 6.5535s*256=1671s ~27.8min delay to write to EEPROM
    //"dec r9\n"
    //"brne Next\n"
    //"dec r10\n"
    //"brne Next\n"
    //"dec r21\n"
    //"cpi r21,1\n"
    //"brne Next\n"
   // blinks
    //"EEWR:\n" //write values to EEPROM
    "cli\n"
    "out 0x18,r1\n" // shut off all leds
    
    "ldi r30,lo8(ee)\n"
    "ldi r31,hi8(ee)\n"
    "ldi r21,0\n" // counter
    "ldi r23,0\n" // sum
    "load:\n"
    "ld r22,Z+\n"
    "add r23,r22\n"
    "cpi r21,5\n"
    "brne 1f\n"
    "mov r22,r23\n" // store sum
    "1:\n"
    "sbic 0x1c,1\n" //wait that EEPE bit is not set
    "rjmp 1b\n"
    "wdr\n"
    "out 0x1c,r1\n" // atomic ERASE and write
    "out 0x1e,r21\n" //EEARL=address
    "out 0x1d,r22\n" // EEDR
    //"in r0,0x3f\n"
    //"cli\n"
    "sbi 0x1c,2\n" // set bit 2 EEMPE
    "sbi 0x1c,1\n" // set bit 1 EEPE to start atomic erase&write to EEPROM    
    //"out 0x3f,r0\n"
    "inc r21\n"
    "cpi r21,6\n"
    "brne load\n"
    "rjmp NextR\n"
  */  
/*    "WriteDefaults:\n"

//    "EEWR:\n" //write values to EEPROM
    "cli\n"
    "out 0x18,r1\n" // shut off all leds
    
    "ldi r30,lo8(ee+1)\n"
    "ldi r31,hi8(ee+1)\n"
    "ldi r21,1\n" // counter
    "load:\n"
    "ldi r22,110\n"
    "st Z+,r22\n"
    "RB:\n"
    "sbic 0x1c,1\n" //wait that EEPE bit is not set
    "rjmp RB\n"
    "wdr\n"
    "out 0x1c,r1\n" // atomic ERASE and write mode
    "out 0x1e,r21\n" //EEARL=address
    "out 0x1d,r22\n" // EEDR
    "sbi 0x1c,2\n" // set bit 2 EEMPE
    "sbi 0x1c,1\n" // set bit 1 EEPE to start atomic erase&write to EEPROM    
    "inc r21\n"
    "cpi r21,5\n"
    "brne load\n"
    "rjmp Next\n"*/
    : : :"r9","r10","r16","r17","r18","r19","r20","r21","r24","r25","r26","r28","r30"); //registers in use
}
// the loop function runs over and over again forever
void loop() {
}
