
//uint8_t volatile state=0;

ISR(TIM0_COMPA_vect,ISR_NAKED) { 
asm volatile(
//"out 0x29,r30\n" // save r30 in OCR0B //"push r30\n"
//"in r30,0x3f\n"//save SREG to EEDR
//"out 0x1d,r30\n"
//"in r30,0x1e\n" // load status from EEARL

"out 0x18,r16\n\t" // central blink 4 clocks 417ns
"out 0x32,r1\n" // tcnt0=0
"inc r28\n"
//"cpi r28,200\n" // 200x0.005ms=1ms
//"cpi r28,240\n" // 240x0.00417ms=1ms
"cpi r28,30\n" // 30x0.0333ms=1ms
"out 0x18,r17\n\t"
"brne 2f\n"
"ldi r28,0\n" // reset ms counter
// every ms

"sbiw r26,1\n" // 65536ms each change from 30 to 11. 65sec x20 ~ 22minutes to full frequency
"brne 3f\n"
"cpi r25,11\n"
"breq 3f\n"
"dec r25\n"
"3:\n"

"inc r30\n"
//"cpi r30,11\n"
"cp r30,r25\n"
"brne 1f\n"
"ldi r30,0\n" // reset state counter
"1:\n"
"cpi r30,1\n"
"brne 1f\n"
"ori r16,0b00000100\n"
"ori r17,0b00000100\n"
"1:\n"
"cpi r30,2\n"
"brne 1f\n"
"andi r16,0b11111011\n"
"andi r17,0b11111011\n"
"1:\n"
"cpi r30,6\n"
"brne 1f\n"
"ori r16,0b00010000\n"
"ori r17,0b00010000\n"
"1:\n"
"cpi r30,7\n"
"brne 1f\n"
"andi r16,0b11101111\n"
"andi r17,0b11101111\n"
"1:\n"
"2:\n"
//"rjmp 1f\n\t1:"// delay 2 clocks
"out 0x18,r17\n\t"

//"out 0x1e,r30\n" //store status into EEARL

//"lds r30"

//"sbis %[IN],%[INPIN]\n" // skip if bit in_pin in port is set (line HIGH)
//"rjmp LOW\n" // jump if in_pin is low (line is low)
//"HIGH:\n"

//"lds r30,(plast)\n" // load last time
//"cpi r30,%[PUMPINT]\n" // check if it is too early to fire a pump
//"brlo exit\n"

//"ldi r30,%[PUMPTIME]\n"
//"sts (PUMPctr),r30\n"
//"sbi %[PORT],%[PUMPpin]\n" //run pump
//"ldi r30,0\n"
//"sts (plast),r30\n" // plast=0
//"ldi r30,0xff\n"
//"sts (pwd),r30\n" // pwd=0xff // reset pump watchdog

//"ldd r30,Z+63\n" //just 2 bytes!!!!!!

//"exit:\n"
//"in r30,0x1d\n"
//"out 0x3f,r30\n"//restore SREG from EEDR
//"in r30,0x29\n" //restore r30 from OCR0B //"pop r30\n"
"reti\n"
//"LOW:\n"
// check minimum time
//"lds r30,(PUMPctr)\n"
//"cpi r30,%[PUMPTIME]\n"
//"breq exit\n" // do nothing if just ON
//"ldi r30,0\n"
//"sts (PUMPctr),r30\n"
//"cbi %[PORT],%[PUMPpin]\n" //stop pump
//"rjmp exit\n"
:::"r16","r17","r18","r19","r30","r31");
}

// the setup function runs once when you press reset or power the board
void setup() {
  ADCSRA &= ~(1<<ADEN); //Disable ADC
  ACSR = (1<<ACD); //Disable the analog comparator
  DIDR0 = 0x3F; //Disable digital input buffers on all pins.
//  TCCR0A=0;
//  TCCR0B=0;
  // setup watchdog for system reset if wdr is not called within 125ms;
 // WDTCR=(1 << WDE)|(1 << WDP1)|(1 << WDP0); //125ms
 WDTCR=(1 << WDE); //16ms between watchdog reset command 
 //WDTCR=(1 << WDE)|(1 << WDP0); //32ms between watchdog reset command 
 //WDTCR=(1 << WDE)|(1 << WDP1); //64ms between watchdog reset command 

 TCCR0A = (1 << WGM01);// CTC mode 
  //TCCR2B = (1 << CS21); // prescaler 8
  //TCCR0B = (1<<CS01)|(1<<CS00); // prescaler 64
  //TCCR0B = 2;//8
  TCCR0B = 3;//64
  //TCCR0B = 4;//256
  //TCCR0B = 5;//1024 9600000/1024=9375=125*75 75 times per second
  TCNT0  = 0;//initialize counter value to 0
  // OCR0A = 99;// = (16*10^6) / (1*8) - 1 (must be <65536)
  //OCR0A = 59; // 2500 times per second
  //OCR0A = 14; // 10000 times per second
  //OCR0A = 149; // 9600000/256=37500 / 150 = 250 times per second (4ms)
  //OCR0A = 37; // 9600000/256=37500 / 38 = 986 times per second (~1ms)
  //OCR0A = 14; // 9600000/64=150000 / 15 = 10000 times per second (~0.1ms)
  //OCR0A = 8; // 9600000/1024=9375 / 9 = 1041 times per second (~1ms)
  //OCR0A = 5; // 9600000/8=1200000 / 6 = 200000 times per second (~5us)
  //OCR0A = 4; // 9600000/8=1200000 / 5 = 240000 times per second (~4.17us)
  OCR0A = 4; // 9600000/64=150000 / 5 = 30000 times per second (~33.3us)
  //OCR0A = 74; // 125 times per second 8ms
MCUCR=0b00100000; // SleepEnable + sleep mode IDLE
cli();
  TIMSK0 = (1 << OCIE0A);

//EEARL=0; //status register

 
asm volatile (
//    "cli\n" // no interrupts from now on
//    "ldi r16,85\n"
    //"clr r25\n"
//    "in r16,0x31\n"
//    "subi r16,-10\n"
//    "out 0x31,r16\n" //set OSCCAL
//    "ldi r16,127\n"
//    "out 0x31,r16\n" //set OSCCAL to maximum

    "ldi r28,0\n" // 0..9 to count ms
    "ldi r30,0\n" // status
    "ldi r16,0b00010101\n\t" // pins 0,2,4 for OUTPUT
    "out 0x17,r16\n\t"
    "ldi r16,0b00000001\n\t"
    "ldi r17,0b00000000\n\t"
    "ldi r26,0\n\t"
    "ldi r27,0\n\t" // r27:r26 dawn counter
    "ldi r25,30\n\t" // starting period 30ms. slowly rising to 11ms.
    "sei\n"    
    "Next:\n\t" 
    "wdr\n"
    "sleep\n" // sleep until next timer interrupt came (~0.01ms)
    "rjmp Next\n"
    //"cli:\n\t""out 0x18,r16\n\t""out 0x18,r17\n\t""sei\n\t""wdr\n\t""rjmp 1f\n\t1:""rjmp 1f\n\t1:""rjmp Next\n\t" // 1/11 is on
  
    
    : : :"r16","r17","r18","r19","r20","r21"); //registers in use
}
// the loop function runs over and over again forever
void loop() {
}
