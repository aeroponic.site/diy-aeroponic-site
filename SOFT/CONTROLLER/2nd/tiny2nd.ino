#include <Arduino.h>
#include <avr/io.h>
#include <avr/wdt.h>
#include <avr/interrupt.h>
#include <avr/sleep.h>

#define FLAG_REG EEARL // this register is used for flags handling

#define LED_pin 3
#define LED_pullup_pin 4
#define PUMP_pin 2
#define IN_PIN 0 // pump trigger input pin

#define PUMP_WATCHDOG 90 // in 4x seconds - so 90 means if there are no pump pulse for 360 seconds(6min) then fire it up anyway

//#define PUMPINTERVAL 20/4 // in 4seconds - 20seconds between pump runs
#define PUMPINTERVAL 116/4 // in 4seconds - 116 seconds minimal gap between pump runs 1min56sec
//#define PUMPMAXTIME 152/4 // in 4ms - 152ms
#define PUMPMAXTIME 200/4 // in 4ms - 200ms // normally main controller is switchimg the pump off by setting pin0 to LOW. but if anything goes wrong - the max time the pump is on is 200ms
uint8_t plast;
uint8_t pwd;
uint8_t PUMPctr=0;

uint8_t t0ctr=0;
void setup() {
  ADCSRA &= ~(1<<ADEN); //Disable ADC
  ACSR = (1<<ACD); //Disable the analog comparator
  DIDR0 = 0x1F; //Disable digital input buffers on all ADC pins.
  // setup watchdog for system reset if wdr is not called within 125ms;
 // WDTCR=(1 << WDE)|(1 << WDP2)|(1 << WDP1)|(1 << WDP0); //2seconds watchdog
 //WDTCR=(1 << WDE)|(1 << WDP3)|(1 << WDP0); //8seconds watchdog
 WDTCR=(1 << WDE); //~16ms watchdog
  //void t2setup(){//set timer2 interrupt at 400microseconds 2500 times per second
  TCCR0A = (1 << WGM01);// CTC mode 
  //TCCR2B = (1 << CS21); // prescaler 8
  //TCCR0B = (1<<CS01)|(1<<CS00); // prescaler 64
  //TCCR0B = 3;//64
  TCCR0B = 4;//256
  //TCCR0B = 5;//1024 9600000/1024=9375=125*75 75 times per second
  TCNT0  = 0;//initialize counter value to 0
  // OCR0A = 99;// = (16*10^6) / (1*8) - 1 (must be <65536)
  //OCR0A = 59; // 2500 times per second
  //OCR0A = 14; // 10000 times per second
  OCR0A = 149; // 9600000/256=37500 / 150 = 250 times per second (4ms)
  //OCR0A = 74; // 125 times per second 8ms
  TIMSK0 = (1 << OCIE0A);

  bitSet(DDRB,LED_pin); // set LED pin for OUTPUT sinking current
  bitSet(DDRB,PUMP_pin); // set pump pin for OUTPUT
  PORTB =0b00000000;
  MCUCR=0b00100000; // SleepEnable + sleep mode IDLE
  
  //MCUCR=0b00100010; // SleepEnable + INT0 on falling edge
  //GIMSK=0b01000000; // INT0 enable

  FLAG_REG=0;
//  set_sleep_mode(SLEEP_MODE_IDLE); // need all the counters and piripherals running
//  sleep_enable();
  //pwd=0xff;// set 17min pump timer (4x255sec=1020sec)
  pwd=PUMP_WATCHDOG;// set 6.5min pump timer (4x100sec=400sec) 6.5min
  
  plast=0xff; // last time was long time ago
  
  bitSet(PCMSK,PCINT0);//pin 0 change interrupt enable 
//GIMSK=0b01100000; // INT0 enable | Pin change INT enable
  //GIMSK=0b01000000; // INT0 enable | Pin change INT disable
  GIMSK=0b00100000; // INT0 disabled | Pin change INT enabled
}


//bool ES=false;
uint8_t volatile CS;//ticks every second

ISR(TIM0_COMPA_vect)// called 250 times per second (every 4ms)
{
  TCNT0=0;//reset counter (not reset by hardware on attiny13)
  wdt_reset();
//  clk++;
  //if((EEARL&1)&&((clk-clks)>58)){bitClear(EEARL,0);nv2=0xAA;ccc=0;} // receiving watchdog (should take 3-4-5 clk)
  if(PUMPctr>0)
  {
    if(--PUMPctr==0){bitClear(PORTB,PUMP_pin);}
  }

  //if(++t0ctr>=9999) //do everysecond here (it may write unsaved regs in sub)
  if(++t0ctr>=249) //do everysecond here (it may write unsaved regs in sub) //1s
  //if(++t0ctr>=249) //do everysecond here (it may write unsaved regs in sub) //1s
  //if(++t0ctr>=124) //do everysecond here (it may write unsaved regs in sub)
  {
    t0ctr=0;
    CS++;
    bitSet(FLAG_REG,0);
    //ES=true; // use FLAGREG bit instead of additional byte
  }
} 

//GPIOR0 0x1e(328)  0x1e EEARL(13) 6bit only
//GPIOR1 0x2a(328)
//GPIOR2 0x2b(328)
//OCR0B 0x28(328) 0x29(13)
//EEDR 0x20(328) 0x1D(13)

ISR(PCINT0_vect,ISR_NAKED) { 
asm volatile(
"out 0x29,r30\n" // save r30 in OCR0B //"push r30\n"
"in r30,0x3f\n"//save SREG to EEDR
"out 0x1d,r30\n"

"sbis %[IN],%[INPIN]\n" // skip if bit in_pin in port is set (line HIGH)
"rjmp LOW\n" // jump if in_pin is low (line is low)
//"HIGH:\n"

"lds r30,(plast)\n" // load last time
"cpi r30,%[PUMPINT]\n" // check if it is too early to fire a pump
"brlo exit\n"

"ldi r30,%[PUMPTIME]\n"
"sts (PUMPctr),r30\n"
"sbi %[PORT],%[PUMPpin]\n" //run pump
"ldi r30,0\n"
"sts (plast),r30\n" // plast=0
"ldi r30,0xff\n"
"sts (pwd),r30\n" // pwd=0xff // reset pump watchdog

//"ldd r30,Z+63\n" //just 2 bytes!!!!!!
      

"exit:\n"
"in r30,0x1d\n"
"out 0x3f,r30\n"//restore SREG from EEDR
"in r30,0x29\n" //restore r30 from OCR0B //"pop r30\n"
"reti\n"
"LOW:\n"
// check minimum time
"lds r30,(PUMPctr)\n"
"cpi r30,%[PUMPTIME]\n"
"breq exit\n" // do nothing if just ON
"ldi r30,0\n"
"sts (PUMPctr),r30\n"
"cbi %[PORT],%[PUMPpin]\n" //stop pump
"rjmp exit\n"
::[PORT]"M"(_SFR_IO_ADDR(PORTB)),[IN]"M"(_SFR_IO_ADDR(PINB)),[INPIN]"M"(IN_PIN),[PUMPINT]"M"(PUMPINTERVAL),[DDR]"M"(_SFR_IO_ADDR(DDRB)),[FLAG]"M"(_SFR_IO_ADDR(FLAG_REG)),[PUMPTIME] "M" (PUMPMAXTIME),[PUMPpin] "M" (PUMP_pin):"r16","r17","r18","r19","r30","r31");
}

void loop() {
 // wdt_reset();

  if(FLAG_REG&1) // every second this bit is set
  {
    bitClear(FLAG_REG,0);//ES=false;
    bitClear(PORTB,LED_pullup_pin);
    if((CS&3)==0) // every 4th second
    {
      if(plast!=255){plast++;}
      if(--pwd==0){pwd=PUMP_WATCHDOG;PUMPctr=PUMPMAXTIME;bitSet(PORTB,PUMP_pin);}// emergency run pump 
      // change led behaviour for emergency

      bitSet(PORTB,LED_pullup_pin); //activate pull up and blink LED 1 second on/3 seconds off
      //if(FLAG_REG&2){bitSet(PORTB,LED_pin);bitClear(FLAG_REG,1);}else{bitClear(PORTB,LED_pin);bitSet(FLAG_REG,1);} //blink LED every 4 seconds
    }
  }
  asm volatile ("sleep":::);
}
