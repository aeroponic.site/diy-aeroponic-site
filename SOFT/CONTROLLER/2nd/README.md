OVERVIEW========================================================================

  Second microcontroller ATTINY13A or ATTINY13V is used to independently control
second pump. It has independent battery backed power supply and if the main 
microcontroller went south this tiny little bugger will render at least one pump
alive and kicking up every 400 seconds. 

OPERATION=======================================================================

  This microcontroller is monitoring pin 0 for a HIGH signal from the main micro
controller. When high signal is there it sets pin 2 to HIGH to start a pump. If 
signal on pin 0 goes LOW then pump on pin 2 signal also goes low. So normally it 
just copies input from main microcontroller. But...

1)If the signal is set HIGH more than 160ms then it will be clipped to 160ms. 
2)If there are no HIGH signal on pin 0 for 400 seconds when it will set pin 2 to 
HIGH for 160ms to run pump on its own.

INSTALLATION====================================================================

The simplest way. If you are agree with defaults then all you need is just flush
already precompiled binary to microcontroller and set it's fuses with correct
values. Install just avrdude (apt install avrdude, pacman install avrdude etc) 
and run the Flush13 script. Correct to COM port in script if your's is different
If all is okay the output on your screen should be similar to result.log, if not
then double check the wires.

The classic way. Here you need to install Arduino IDE.
To compile programs for attiny13 we need to set up additional board manager 
called MicroCore. In the menu File > Preferences > Additional board manager URLs 
just add this path
    https://mcudude.github.io/MicroCore/package_MCUdude_MicroCore_index.json
and open the Tools > Board > Boards Manager menu item.
Wait for the platform indexes to finish downloading.
Scroll down until you see the MicroCore entry and click on it.
Click Install.
After installation select the Attiny13 in Tools > Board menu and make sure the
defaults are correctly set: BOD -2.7v, LTO enabled, Clock 9.6MHz internal osc.

TBD